# Gadget Buying Tips - 4 Facts You Need to Remember #

Okay, so you saw one of those nifty little gadgets on one of your trips, would you instantly buy it?

If you're a technophile, then the answer might be a resounding "YES!" (Also, if you're impulsive, it might be "YES!" too.) Take a step back for a while. Here are some important facts to consider before getting that walking cellphone.

1. Is it supported where you're from? Does your country have the necessary services to allow such a device? There's this story about cellphones - Japan has some of the slimmest, most-feature-packed phones way before WAP and WiFi were the orders of the day in today's models. Take note, however, that phones from Japan come bundled with a subscription service, and that they don't use GSM. What this basically means is that, if your country has GSM deployed for cellular phone services, a phone you bought in Japan would have a slim, if none, chances of working. Or game consoles that only have outputs that your TV doesn't support, that sort of thing. There are hacks/unlocks for other devices, but that would mean you're breaking the terms of use bundled with the device. In a manner of speaking, it's breaking the law of devices.

2. Warranty. If, say, you're in a country where so-and-so brand originated, and they sell plasma TVs WITH warranty at a lower price, it doesn't automatically mean you're covered when you leave that country.Say, you bought an uber-cool cell phone-slash-camera-slash-music player-slash-game console and it breaks - there's little to no chance of that ever getting fixed if it's manufacturer doesn't have a base in your country, and, if it ever did, don't count on your original warranty to matter when you're getting it fixed.

3. Read the fine print, or, get someone to read it for you if it's in another language. Say you get this thumb-sized music player for a really low price, but upon getting home, you realize that the official adapter used to charge the batteries isn't included ... that's bad. Make sure you know what you're getting and what getting that device entails upon purchasing.

4. Do your research. Just like buying a device, if possible, do your research online. If the gadget is too obscure, physically test the device on-site, if you can, or compare it with other gadgets in the store. Another thing you could do is ask for opinions from other customers, or, as a last resort, from the clerk who's trying to make a sell. There have been times that a sales clerk would be more concerned for your welfare as a consumer, rather than caring if he makes a sell or not. Look for these types of people. They will help you.

Hopefully, these tips have enlightened you before your purchase. Browsing stores on different shores is always a fun thing, you never know whether back-alley sales have any difference from the ones in the bigger stores.

Getting a gadget that would turn into a useless heap of plastic and metal once you land? That's a hassle. Being smart about your budget? That's priceless.

[https://digitogy.com/ja/](https://digitogy.com/ja/)

